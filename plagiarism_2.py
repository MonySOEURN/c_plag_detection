import sys, os
from PySide2 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import (QMainWindow, QApplication, QWidget, QFileDialog, QTextEdit, QPushButton,
                             QLabel, QVBoxLayout, QHBoxLayout, QFrame, QToolButton )
from PyQt5.QtGui import QPixmap, QPolygon
from PyQt5.QtCore import QDir, QFileInfo, QPoint, QFile, QIODevice, QDataStream, QVariant, Qt
from PyQt5 import  uic

import main
import ast
import pycparser 
# from pycparser import c_parser
from astmonkey import visitors, transformers
import parser 
import pprint
from graph_tools import *
import pydot
from PIL.ImageQt import ImageQt

class PlagiarismApp(QMainWindow):
    input_file_1 = None
    input_file_2 = None

    def __init__ (self):
        super(PlagiarismApp, self).__init__()
        uic.loadUi("main.ui", self)

        # button for upload
        self.toolButton_3.clicked.connect(self.open_file_1)
        self.toolButton_2.clicked.connect(self.open_file_2)

        self.pushButton.clicked.connect(self.generate_ast)

    # upload events
    def open_file_1 (self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', "/Users/monysoeurn/Desktop/cpp","files (*.c *.cpp, *.py)")
        fi = QFileInfo(str(fname[0]))
        self.input_file_1 = fi
        self.textBrowser_4.setPlainText(fi.fileName())

    def open_file_2 (self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', "/Users/monysoeurn/Desktop/cpp","files (*.c *.cpp, *.py)")
        fi = QFileInfo(str(fname[0]))
        self.input_file_2 = fi
        self.textBrowser_2.setPlainText(fi.fileName())

    def make_function_call_graph(self, node, edge, graph):
        if(node.name.name == 'scanf'):
            head2_ast = "input\n{}: {}".format(node.args.exprs[0].type, node.args.exprs[1].expr.name)
            edge = pydot.Edge(edge, head2_ast)
            graph.add_edge(edge)
        if(node.name.name == 'printf'):
            head2_ast = "output\n{}: {}".format(node.args.exprs[0].type, node.args.exprs[0].value)
            edge = pydot.Edge(edge, head2_ast)
            graph.add_edge(edge)

    def make_return_graph(self, node, edge, graph):
        head2_ast = "Return"
        edge = pydot.Edge(edge, head2_ast)
        graph.add_edge(edge)

        head3_ast = "variable\n constant: {}".format(node.expr.value)
        edge = pydot.Edge(head2_ast, head3_ast)
        graph.add_edge(edge)
    
    def make_condition_graph(self, node, edge, graph):
        if(type(node.cond.left) is pycparser.c_ast.ID):
            head4_ast = "Lvariable\nname: {}".format(node.cond.left.name)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)
        else: 
            head4_ast = "Lvariable\nconstant: {}".format(node.cond.left.value)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)

        if(type(node.cond.right) is pycparser.c_ast.ID):
            head4_ast = "Rvariable\nname: {}".format(node.cond.right.name)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)
        else:
            head4_ast = "Rvariable\nconstant: {}".format(node.cond.right.value)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)

    def make_unary_op_graph(self, node, edge, graph):
        head4_ast = "Increment\n{}: {}".format(node.expr.name, node.op)
        tmp_edge = pydot.Edge(edge, head4_ast)
        graph.add_edge(tmp_edge)

    def make_assignment_graph(self, node, edge, graph):
        if(type(node.lvalue) is pycparser.c_ast.ID):
            head4_ast = "A.Lvariable\nname: {}".format(node.lvalue.name)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)
        else: 
            head4_ast = "A.Lvariable\nconstant: {}".format(node.lvalue.value)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)

        if(type(node.rvalue) is pycparser.c_ast.ID):
            head4_ast = "A.Rvariable\nname: {}".format(node.rvalue.name)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)
        else:
            head4_ast = "A.Rvariable\nconstant: {}".format(node.rvalue.value)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)

    def make_binary_op_graph(self, node, edge, graph):
        if(type(node.left) is pycparser.c_ast.ID):
            head4_ast = "F.Lvariable\nname: {}".format(node.left.name)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)
        else: 
            head4_ast = "F.Lvariable\nconstant: {}".format(node.left.value)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)

        if(type(node.right) is pycparser.c_ast.ID):
            head4_ast = "F.Rvariable\nname: {}".format(node.right.name)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)
        else:
            head4_ast = "F.Rvariable\nconstant: {}".format(node.right.value)
            tmp_edge = pydot.Edge(edge, head4_ast)
            graph.add_edge(tmp_edge)



    # generate AST view
    def generate_ast(self):
        # print("clicked generate")
        with open(self.input_file_1, "r") as file:
            # print(file.name)
            my_ast1 = pycparser.parse_file(file.name)
            # print(my_ast1)

            # first you create a new graph, you do that with pydot.Dot()
            ast_1_graph = pydot.Dot(graph_type='graph')
            for x in my_ast1.ext:
            #     # for header function declaration, this can represent as Main
                # print(x.decl.type.type.declname)
            #     print(x.decl.type.type.type.names[0])

            #     # the list of functionn that we use, 
                for y in x.body.block_items:
                    head_ast = "{}\n{}".format(x.decl.type.type.declname, x.decl.type.type.type.names[0])
            #         # to access function call of "printf"

                    # classify into different type of condition

                    # graph for Declaration
                    if(type(y) is pycparser.c_ast.Decl):
                        # print(y)
                        head2_ast = "variable\nname: {}".format(y.name)
                        edge = pydot.Edge(head_ast, head2_ast)
                        ast_1_graph.add_edge(edge)

                    # graph for FunctionCall
                    if(type(y) is pycparser.c_ast.FuncCall):
                        self.make_function_call_graph(y, head_ast, ast_1_graph)

                    # graph for IF statement
                    if(type(y) is pycparser.c_ast.If):
                        # print(y)
                        head2_ast = "statment\nIF:"
                        edge = pydot.Edge(head_ast, head2_ast)
                        ast_1_graph.add_edge(edge)

                        head3_ast = "condition\nop:{}".format(y.cond.op)
                        edge = pydot.Edge(head2_ast, head3_ast)
                        ast_1_graph.add_edge(edge)

                        # condition graph 
                        if(type(y.cond.left) is pycparser.c_ast.ID):
                            head4_ast = "Lvariable\nname: {}".format(y.cond.left.name)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_1_graph.add_edge(edge)
                        else: 
                            head4_ast = "Lvariable\nconstant: {}".format(y.cond.left.value)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_1_graph.add_edge(edge)

                        if(type(y.cond.right) is pycparser.c_ast.ID):
                            head4_ast = "Rvariable\nname: {}".format(y.cond.right.name)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_1_graph.add_edge(edge)
                        else:
                            head4_ast = "Rvariable\nconstant: {}".format(y.cond.right.value)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_1_graph.add_edge(edge)
                        
                        # True statment body graph
                        if(type(y.iftrue) is pycparser.c_ast.FuncCall):
                            self.make_function_call_graph(y.iftrue, head2_ast, ast_1_graph)

                        # False statement body graph
                        if(type(y.iffalse) is pycparser.c_ast.FuncCall):
                            self.make_function_call_graph(y.iffalse, head2_ast, ast_1_graph)

                    # graph for Return statement   
                    if(type(y) is pycparser.c_ast.Return):
                        # print(y)
                        self.make_return_graph(y, head_ast, ast_1_graph)

                    # graph for WHILE statement
                    if(type(y) is pycparser.c_ast.While):
                        # print(y)
                        head2_ast = "statment\nWHILE:"
                        edge = pydot.Edge(head_ast, head2_ast)
                        ast_1_graph.add_edge(edge)

                        head3_ast = "condition\nop:{}".format(y.cond.op)
                        edge = pydot.Edge(head2_ast, head3_ast)
                        ast_1_graph.add_edge(edge)

                        # condition graph
                        self.make_condition_graph(y, head3_ast, ast_1_graph)

                        # statment body graph
                        if(type(y.stmt.block_items[0]) is pycparser.c_ast.FuncCall):
                            self.make_function_call_graph(y.stmt.block_items[0], head2_ast, ast_1_graph)

                        # statment body graph
                        if(type(y.stmt.block_items[1]) is pycparser.c_ast.UnaryOp):
                            self.make_unary_op_graph(y.stmt.block_items[1], head2_ast, ast_1_graph)

                    # graph for FOR statement
                    if(type(y) is pycparser.c_ast.For):
                        # print(y)
                        # assignment graph
                        if (type(y.init) is pycparser.c_ast.Assignment):
                            head2_ast = "statement\nFOR:"
                            edge = pydot.Edge(head_ast, head2_ast)
                            ast_1_graph.add_edge(edge)

                            head3_ast = "assign\nop: {}".format(y.init.op)
                            edge = pydot.Edge(head2_ast, head3_ast)
                            ast_1_graph.add_edge(edge)
                            self.make_assignment_graph(y.init, head3_ast, ast_1_graph)

                        # condition graph
                        if (type(y.cond) is pycparser.c_ast.BinaryOp):
                            head3_ast = "condition\nop: {}".format(y.init.op)
                            edge = pydot.Edge(head2_ast, head3_ast)
                            ast_1_graph.add_edge(edge)
                            self.make_binary_op_graph(y.cond, head3_ast, ast_1_graph)

                        # next operation graph    
                        if (type(y.next) is pycparser.c_ast.UnaryOp):
                            self.make_unary_op_graph(y.next, head2_ast, ast_1_graph)

                        # body statement graph
                        if (type(y.stmt) is pycparser.c_ast.FuncCall):
                            print(y.stmt)
                            self.make_function_call_graph(y.stmt, head2_ast, ast_1_graph)


            # # create graph into png format
            ast_1_graph.write_png('example1_graph.png')
            # convert ast into strinng by place into file
            # ast_buffer_write=open("store_file1_AST.txt","w") # here will be your AST source
            # my_ast1.show(ast_buffer_write)
            # ast_buffer_write.close()
            # ast_buffer_read=open("store_file1_AST.txt","r")
            # astString=ast_buffer_read.read()
        self.label_5.setPixmap(QPixmap('example1_graph.png').scaled(self.label_5.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))

            # node = ast.parse('def foo(x):\n\treturn x + 1')
            # node = transformers.ParentChildNodeTransformer().visit(node)
            # visitor = visitors.GraphNodeVisitor()
            # visitor.visit(node)
            # visitor.graph.write_png('graph.png')

        with open(self.input_file_2, "r") as file:
            my_ast2 = pycparser.parse_file(file.name)# first you create a new graph, you do that with pydot.Dot()
            ast_2_graph = pydot.Dot(graph_type='graph')
            for x in my_ast2.ext:
            #     # for header function declaration, this can represent as Main
                # print(x.decl.type.type.declname)
            #     print(x.decl.type.type.type.names[0])

            #     # the list of functionn that we use, 
                for y in x.body.block_items:
                    head_ast = "{}\n{}".format(x.decl.type.type.declname, x.decl.type.type.type.names[0])
            #         # to access function call of "printf"

                    # classify into different type of condition

                    # graph for Declaration
                    if(type(y) is pycparser.c_ast.Decl):
                        # print(y)
                        head2_ast = "variable\nname: {}".format(y.name)
                        edge = pydot.Edge(head_ast, head2_ast)
                        ast_2_graph.add_edge(edge)

                    # graph for FunctionCall
                    if(type(y) is pycparser.c_ast.FuncCall):
                        self.make_function_call_graph(y, head_ast, ast_2_graph)

                    # graph for IF statement
                    if(type(y) is pycparser.c_ast.If):
                        # print(y)
                        head2_ast = "statment\nIF:"
                        edge = pydot.Edge(head_ast, head2_ast)
                        ast_2_graph.add_edge(edge)

                        head3_ast = "condition\nop:{}".format(y.cond.op)
                        edge = pydot.Edge(head2_ast, head3_ast)
                        ast_2_graph.add_edge(edge)

                        # condition graph 
                        if(type(y.cond.left) is pycparser.c_ast.ID):
                            head4_ast = "variable\nname: {}".format(y.cond.left.name)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_2_graph.add_edge(edge)
                        else:
                            head4_ast = "variable\nconstant: {}".format(y.cond.left.value)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_2_graph.add_edge(edge)
                        if(type(y.cond.right) is pycparser.c_ast.ID):
                            head4_ast = "variable\nname: {}".format(y.cond.right.name)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_2_graph.add_edge(edge)
                        else:
                            head4_ast = "variable\nconstant: {}".format(y.cond.right.value)
                            edge = pydot.Edge(head3_ast, head4_ast)
                            ast_2_graph.add_edge(edge)
                        
                        # True statment body graph
                        if(type(y.iftrue) is pycparser.c_ast.FuncCall):
                            self.make_function_call_graph(y.iftrue, head2_ast, ast_2_graph)

                        # False statement body graph
                        if(type(y.iffalse) is pycparser.c_ast.FuncCall):
                            self.make_function_call_graph(y.iffalse, head2_ast, ast_2_graph)

            # # create graph into png format
            ast_2_graph.write_png('example2_graph.png')
        self.label_6.setPixmap(QPixmap('example2_graph.png').scaled(self.label_6.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = PlagiarismApp()
    w.show()  # show window
    sys.exit(app.exec_())